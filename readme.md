# Moder GUI Development with Python Flask & HTML
This was made in the course of [Udemy](https://www.udemy.com/course/flask-gui/).

# Pre-instalation
* python3.10

# Instalation
* pip install -r requirements
* pyinstaller --name="CPU-Meater" --onefile --paths=env --add-data="static:static" --add-data="templates:templates" --noconsole --icon=favicon.ico app.py
